<?php

use App\User;
use Illuminate\Http\Response;
use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;
use Illuminate\Contracts\Auth\Authenticatable;
use Tymon\JWTAuth\JWTAuth;

class AuthTest extends TestCase
{
    public function token()
    {
        $token = $this->headers(\App\User::first());

        return $token;
    }
    public function testLogin()
    {
        $response = $this->call('POST', '/login',
            $user = [
                'username' => App\User::all()->first(),
                'password' => '123456',
            ]
        );

        $this->assertEquals(200, $response->status());
    }

    public function testRegister()
    {
        $response = $this->call('POST', '/register');
            $user = factory('App\User')->make();

        $this->assertEquals(200, $response->status());

    }

    public function testCheckLogin()
    {
        $response = $this->get('/users/check-login',
                    $this->token());
                    $this->seeJson([
                        'message' => 'authenticated_user'],
                    $response);

    }

    public function testGetAllUsers()
    {
        $response = $this->get('/users', $this->token())
                    ->assertResponseStatus(200);
    }

    public function testGetUsersById()
    {
        $user = User::get()->random()->id;

        $response = $this->get('/users/show/'.$user, $this->token())
                    ->assertResponseStatus(200);
    }

    public function testRefreshToken()
    {
        $response = $this->call('POST', '/users/refresh',
                    $this->token());

        $this->assertEquals(401, $response->status());
    }


}
