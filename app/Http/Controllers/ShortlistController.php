<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\service\ServiceList;

class ShortlistController extends Controller
{


    public function __construct(ServiceList $service)
    {
        $this->service = $service;
    }

    /**
    * @SWG\Get(
    *   path="/shortlist",
    *   summary="Get list",
    *   tags={"short-list"},
    *
    *   @SWG\Response(
    *     response=200,
    *     description="Return User"
    *   ),
    *
    *   @SWG\Response(
    *     response="400",
    *     description="Error"
    *   )
    * )
    *
    * @SWG\Tag(name="short-list")
    */
    public function shortList()
    {
        $data = [
            'msg' => 'success get data',
            'data' => explode("\r\n", $this->service->get())
        ];

        return response()->json($data, 200);
    }
}