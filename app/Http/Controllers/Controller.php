<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;

/**
 * @SWG\Swagger(
 *     basePath="",
 *     schemes={"http"},
 *     @SWG\Info(
 *         version="0.0.1",
 *         title="API documentation",
 *         @SWG\Contact(
 *             email="myname@ahmadsaubani.com"
 *         ),
 *     )
 * )
 */
class Controller extends BaseController
{
    //
}
