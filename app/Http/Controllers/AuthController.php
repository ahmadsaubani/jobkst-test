<?php

namespace App\Http\Controllers;

use Auth;
use Validator;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Tymon\JWTAuth\JWTAuth;
use Illuminate\Support\Facades\Mail;
use App\Mail\EmailVerification;
use Swagger\Annotations as SWG;

class AuthController extends Controller
{
    /**
     * @var \Tymon\JWTAuth\JWTAuth
     */
    protected $jwt;

    public function __construct(JWTAuth $jwt)
    {
        $this->jwt = $jwt;
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($jwt = [])
    {
        return response()->json([
            'access_token' => $jwt['token'],
            'token_type' => 'bearer',
            'expires_in' => Auth::factory()->getTTL()
        ]);
    }

    /**
    * @SWG\Post(
    *   path="/login",
    *   summary="Login User",
    *   tags={"Auth"},
    *
    *   @SWG\Parameter(name="username",in="query",
    *     description="Required Username",
    *     required=true, type="string",
    *     @SWG\Items(type="string")
    *   ),
    *   @SWG\Parameter(name="password",
    *     in="query",
    *     description="Required Password",
    *     required=true, type="string",
    *     @SWG\Items(type="string")
    *   ),
    *   @SWG\Response(
    *     response=200,
    *     description="Working"
    *   ),
    *
    *   @SWG\Response(
    *     response="400",
    *     description="error"
    *   )
    * )
    *
    */
    public function login(Request $request)
    {
        $this->validate($request, [
            'username' => 'required|max:255',
            'password' => 'required',
        ]);

        $credentials = $request->only(['username', 'password']);

        try {

        if (! $token = Auth::attempt($credentials)) {
            return response()->json(['error' => 'Bad Credentials'], 401);
        }

        } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

            return response()->json(['Token Expired'], 500);

        } catch (\Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

            return response()->json(['Invalid Token'], 500);

        } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {

            return response()->json(['Absent Token' => $e->getMessage()], 500);

        }

        $user = Auth::user();

        $user->token = $token;

        $user->save();

        return $this->respondWithToken(['token' => $token]);
    }

    /**
     * User Register
     *
     * @param string $email
     * @param string $name
     * @param string $password
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255|unique:users',
            'name' => 'required',
            'password'=> 'required',
            'username' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors());
        }
        $user =  User::create([
            'name' => $request->get('name'),
            'username' => $request->get('username'),
            'email' => $request->get('email'),
            'password' => Hash::make($request->get('password')),
        ]);

        Mail::to($request->email)->send(new EmailVerification($user));

        return response()->json($user, 201);
    }

    /**
    * @SWG\Get(
    *   path="/users/show/{id}",
    *   summary="Get All Users",
    *   tags={"Auth"},
    *
    *  @SWG\Parameter(name="Authorization",in="header",
    *     description="Required Authorization",
    *     required=true, type="string",
    *     @SWG\Items(type="string")
    *   ),
    *
    *   @SWG\Parameter(name="id",in="path",
    *     description="Required ID",
    *     required=true, type="string",
    *     @SWG\Items(type="string")
    *   ),
    *
    *   @SWG\Response(
    *     response=200,
    *     description="Return User"
    *   ),
    *
    *   @SWG\Response(
    *     response="400",
    *     description="Error"
    *   )
    * )
    *
    * @SWG\Tag(name="Auth")
    */
    public function getUserById($id)
    {
        $user = User::find($id);
        if($user === null) {
            return response()->json([
                'message'=> 'User Not Found'], 404);
            }
        return response()->json($user, 200);
    }

    /**
    * @SWG\Get(
    *   path="/users",
    *   summary="Get All Users",
    *   tags={"Auth"},
    *
    *  @SWG\Parameter(name="Authorization",in="header",
    *     description="Required Authorization",
    *     required=true, type="string",
    *     @SWG\Items(type="string")
    *   ),
    *
    *   @SWG\Response(
    *     response=200,
    *     description="Return User"
    *   ),
    *
    *   @SWG\Response(
    *     response="400",
    *     description="Error"
    *   )
    * )
    *
    * @SWG\Tag(name="Auth")
    */
    public function getAllUser()
    {
        $user = User::all();

        return  response()->json($user, 200);
    }

    /**
    * @SWG\Get(
    *   path="/users/check-login",
    *   summary="Check Your Account",
    *   tags={"Auth"},
    *
    *  @SWG\Parameter(name="Authorization",in="header",
    *     description="Required Authorization",
    *     required=true, type="string",
    *     @SWG\Items(type="string")
    *   ),
    *
    *
    *   @SWG\Response(
    *     response=200,
    *     description="Return User"
    *   ),
    *
    *   @SWG\Response(
    *     response="400",
    *     description="Error"
    *   )
    * )
    *
    * @SWG\Tag(name="Auth")
    */
    public function me()
    {
        $user = Auth::user();

        if(! $user)
        {
            return response()->json(['msg' => 'Error'], 400);
        }
        return response()->json($user, 200);
    }

    /**
    * @SWG\Put(
    *   path="/users/change",
    *   summary="Change Password",
    *   tags={"Auth"},
    *
    *  @SWG\Parameter(name="Authorization",in="header",
    *     description="Required Authorization",
    *     required=true, type="string",
    *     @SWG\Items(type="string")
    *   ),
    *
    *   @SWG\Parameter(name="password",in="query",
    *     description="Required Password",
    *     required=true, type="string",
    *     @SWG\Items(type="string")
    *   ),
    *
    *   @SWG\Response(
    *     response=200,
    *     description="Return User"
    *   ),
    *
    *   @SWG\Response(
    *     response="400",
    *     description="Error"
    *   )
    * )
    *
    * @SWG\Tag(name="Auth")
    */
    public function update(Request $request)
    {
        $user = Auth::user();

            if(!$user){
                return response()->json(['message' => "Failed Update Password"], 404);
            }
                $this->validate($request, [
                $user->email        = $user->email,
                $user->password     = Hash::make($request->password)
                ]);

        $user->save();
        Auth::logout(true);
        return response()->json(['data' => $user, 'msg' =>  'Change Password Success'],200);
    }

    /**
    * @SWG\Delete(
    *   path="/users/delete/{id}",
    *   summary="Delete User",
    *   tags={"Auth"},
    *
    *  @SWG\Parameter(name="Authorization",in="header",
    *     description="Required Authorization",
    *     required=true, type="string",
    *     @SWG\Items(type="string")
    *   ),
    *
    *   @SWG\Parameter(name="id",in="path",
    *     description="Required Id",
    *     required=true, type="integer",
    *     @SWG\Items(type="integer")
    *   ),
    *
    *   @SWG\Response(
    *     response=200,
    *     description="Return User"
    *   ),
    *
    *   @SWG\Response(
    *     response="400",
    *     description="Error"
    *   )
    * )
    *
    * @SWG\Tag(name="Auth")
    */
    public function delete($id)
    {
        $user = User::find($id);

        if(!$user){
            return response()->json(['msg' => "User Not Found"], 404);
        }

        $user->delete();

        return response()->json(['msg' => "Delete Success"], 200);
    }

    /**
    * @SWG\Post(
    *   path="/users/logout",
    *   summary="User Logout",
    *   tags={"Auth"},
    *
    *  @SWG\Parameter(name="Authorization",in="header",
    *     description="Required Authorization",
    *     required=true, type="string",
    *     @SWG\Items(type="string")
    *   ),
    *
    *   @SWG\Response(
    *     response=200,
    *     description="Logout Success"
    *   ),
    *
    *   @SWG\Response(
    *     response="400",
    *     description="Error"
    *   )
    * )
    *
    * @SWG\Tag(name="Auth")
    */
    public function logout()
    {
        $user = Auth::logout(true);

        return response()->json(['msg' => 'success logout'], 200);
    }

    /**
    * @SWG\Post(
    *   path="/users/refresh",
    *   summary="Refresh Token",
    *   tags={"Auth"},
    *
    *  @SWG\Parameter(name="Authorization",in="header",
    *     description="Required Authorization",
    *     required=true, type="string",
    *     @SWG\Items(type="string")
    *   ),
    *
    *   @SWG\Response(
    *     response=200,
    *     description="Refresh Token Success"
    *   ),
    *
    *   @SWG\Response(
    *     response="400",
    *     description="Error"
    *   )
    * )
    *
    * @SWG\Tag(name="Auth")
    */
    public function refresh(Request $request)
    {
        $user = Auth::user();

        return $this->respondWithToken(['token' => Auth::refresh($user, true, true)]);
    }

    /**
     * Verify Email
     *
     */
    public function verifyEmail(Request $request)
    {

        $ref = $request->get('ref');
        $ref = explode('|', decrypt($ref));

        $user = User::find($ref[1]);

        $user->update([
            'email_verified' => true,
            'email_verified_at' => \Carbon\Carbon::now()->toDateTimeString()
        ]);

        return $user;
    }

}
