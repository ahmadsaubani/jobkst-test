<?php

namespace App\service;

class ServiceList
{
    public function get()
    {
        $rows = [];
        $skeys = [];
        $result = '';
        $data = file('storage/unsorted-names-list.txt');

        $i =0;
        foreach($data as $key => $val)
        {
            $rowarray = explode(" ", $val);

            try{
                $keys = trim($rowarray[2]. $i);

                $rows[$keys] = $val;
                $skeys[] = $keys;

            }catch(\Exception $e) {
                return response()->json(['message' => 'error please report to our developer'], 400);
            }

            $i++;
        }

        natsort($skeys);

        foreach($skeys as $key => $val) {
            $result .= trim($rows[$val]) . "\r\n";
        }

        file_put_contents('storage/sorted-names-list.txt', $result);

        return $result;
    }
}