<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name'     => $faker->name,
        'username' => $faker->userName,
        'email'    => $faker->unique()->freeEmail,
        'phone_number' => $faker->unique()->phoneNumber,
        'email_verified' => 1,
        'email_verified_at' => $faker->dateTime($max='now'),
        'password' => password_hash('123456', PASSWORD_BCRYPT)
    ];
});